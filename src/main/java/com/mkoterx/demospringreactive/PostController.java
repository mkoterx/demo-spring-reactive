package com.mkoterx.demospringreactive;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@CrossOrigin
public class PostController {

    private final PostRepository repository;

    public PostController(PostRepository repository) {
        this.repository = repository;
    }

    @GetMapping(value = "/posts", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    Flux<Post> list() {
        return this.repository.getPostsStreamed().log();
    }

    @GetMapping("/posts/{id}")
    Mono<Post> findById(@PathVariable Long id) {
        return this.repository.findById(id);
    }
}
