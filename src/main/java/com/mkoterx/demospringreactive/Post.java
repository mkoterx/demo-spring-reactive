package com.mkoterx.demospringreactive;

import java.time.LocalDateTime;

public class Post {

    private long id;
    private String title;
    private String desc;
    private LocalDateTime datePublished;
    private String author;

    Post(long id, String title, String desc, LocalDateTime datePublished, String author) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.datePublished = datePublished;
        this.author = author;
    }

    long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public LocalDateTime getDatePublished() {
        return datePublished;
    }

    public String getAuthor() {
        return author;
    }
}
