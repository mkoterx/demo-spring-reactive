package com.mkoterx.demospringreactive;

import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

@Repository
public class PostRepository {

    private static Post[] posts = {
            new Post(1, "Title 1", "Description 1", LocalDateTime.now(), "Mark Zerg"),
            new Post(2, "Title 2", "Description 2", LocalDateTime.now(), "David Schule"),
            new Post(3, "Title 3", "Description 3", LocalDateTime.now(), "Martin Gracham"),
            new Post(4, "Title 4", "Description 4", LocalDateTime.now(), "Iva Miova"),
    };

    Flux<Post> findAll() {
        return Flux.just(posts);
    }

    Mono<Post> findById(long id) {
        return Mono.just(Arrays.stream(posts).filter(p -> Long.compare(id, p.getId()) == 0).limit(1).reduce((a, b) -> null).get());
    }

    Flux<Post> getPostsStreamed() {
        return Flux.interval(Duration.ofSeconds(2))
                .map(i -> new Post(i, "Title " + i, "Description " + i, LocalDateTime.now(), randomAuthor()));
    }

    private String randomAuthor() {
        String[] authors = "Martin,David,Rothams,Julia,Jessica,Victor,Boris,Alba".split(",");
        return authors[new Random().nextInt(authors.length)];
    }
}
